<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionEjercicio1(){
        $alumnos= ['Mario','Ana','Maheva','Denis'];
        
        return $this->render('ejercicio1',[
            "alumnos"=> $alumnos
        ]);
    }
    
    public function actionEjercicio2(){
        $fotos=["a.png","b.png","c.png"];
        
        return $this->render('ejercicio2',[
            "imagenes" => $fotos
        ]);
    }
    
    public function actionEjercicio3($numero=0){
        $alumnos=[
            [
                "nombre" => "Ana",
                "poblacion" => "Santoña",
                "imagen" => "a.png"
            ],
            [
                "nombre" => "Maheva",
                "imagen" => "b.png",
                "poblacion" => "Santoña",
            ],
            [
                "nombre" => "Alberto",
                "imagen" => "c.png",
                "poblacion" => "Santoña",
            ]
        ];
        
               
        return $this->render('ejercicio3',[
            "alumno" => $alumnos[$numero]
        ]);
    }
    
    public function actionEjercicio4(){
        $numeros=[1,2,3,4,5,6,7,8,9];
        
        return $this->render('ejercicio4',[
            "datos" => $numeros
        ]);
    }
    

    
    
}
