<?php
use yii\helpers\Html;

foreach ($imagenes as $imagen) {
    echo Html::img("@web/imgs/{$imagen}",[
        "class" => "col-4"
    ]);
}
/*
 * otra forma de mostrar las imagenes
 */
//echo Html::ul($imagenes,[
//    'item' => function($nombre,$indice){
//       return Html::img("@web/imgs/{$nombre}",[
//        "class" => "col-4"
//    ]);
//    }
//]);




