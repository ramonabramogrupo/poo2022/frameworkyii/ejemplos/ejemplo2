<?php
use yii\helpers\Html;

echo Html::ul($datos,[
    // un clase al ul
    "class" => "list-group list-group-horizontal" , 
    "itemOptions" => [
        // una clase a cada li
        "class" => "list-group-item" 
    ]
]);

